 ## Objective is to predict the total count of bikes rented during each hour
library(ggplot2)
## Load data and check dataframe
df <- read.csv("bikeshare.csv")
print(head(df))

## Scatter Plots
ggplot(df,aes(temp,count)) + geom_point(alpha=0.2, aes(color=temp)) + theme_bw()

## Convert datetime in POSIX
df$datetime <- as.POSIXct(df$datetime)
ggplot(df,aes(datetime,count)) + geom_point(alpha=0.2, aes(color=temp)) + scale_color_continuous(low='#000000',high='#FF6555') 


## Quick Overview
## Seasonality to the data, for winter and summer. Bike rental counts are increasing.

# Boxplot -> y axis - count , x axis - box/season

plot <- ggplot(df, aes(factor(season),count))
plot + geom_boxplot(aes(fill = factor(season))) + theme_bw()

# Distinct hours in a new column
df$hours <- format(df$datetime, "%H") 

# Working days scatter
pl <- ggplot(filter(df,workingday==1),aes(hours,count))
pl <- pl + geom_point(position=position_jitter(w=1, h=0),aes(color=temp),alpha=0.5)
pl <- pl + scale_color_gradientn(colours = c('black','blue','light blue','light green','yellow','orange','red'))
pl + theme_bw()

# Non Working days scatter
pl <- ggplot(filter(df,workingday==0),aes(hours,count))
pl <- pl + geom_point(position=position_jitter(w=1, h=0),aes(color=temp),alpha=0.5)
pl <- pl + scale_color_gradientn(colours = c('black','blue','light blue','light green','yellow','orange','red'))
pl + theme_bw()

# Build a Linear Regression Model
temp.model <- lm(count~temp,df)
summary(temp.model)

# Intercept = 6.04 (the value of y when x=0)
# Estimated number of rental when temperature is 0 C

# Temp = 9.17
# Slope is 9.17 , change in y / change in x
# 1 degree C equals an increase of 9.17 bikes


# Predict bike rentals when temp is 25C

#1
6.04 + 9.17*25 # = 235 bikes

#2 
temp.test <- data.frame(temp=c(25))
predict(temp.model,temp.test)

df$hours <- sapply(df$hours,as.numeric)
df$hours

