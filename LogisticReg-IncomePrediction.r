#### Libraries ####
library(caTools)
library(ggplot2)
library(dplyr)
library(Amelia)

adult <- read.csv('data/adult_sal.csv')
print(head(adult))

## Data Munging #####
## Try to reduce the number of unecessary factors

print(summary(adult))
adult <- select(adult, -X)
table(adult$type_employer)

## Combine the following groups to a new one
unemp <- function(job){
  job <- as.character(job)
  if (job == 'Never-worked' | job == 'Without-pay'){
    return('Unemployed')
  }else{
    return(job)
  }
}

adult$type_employer <- sapply(adult$type_employer,unemp)

## Group self employed and state and local ### 

group_emp <- function(job){
  if (job=='Local-gov' | job=='State-gov'){
    return('SL-gov')
  }else if (job=='Self-emp-inc' | job=='Self-emp-not-inc'){
    return('self-emp')
  }else{
    return(job)
  }
}

adult$type_employer <- sapply(adult$type_employer,group_emp)
print(table(adult$type_employer))

### Marital Status ###

group_marital <- function(mar){
  mar <- as.character(mar)
  
  # Not-Married
  if (mar=='Separated' | mar=='Divorced' | mar=='Widowed'){
    return('Not-Married')
    
    # Never-Married   
  }else if(mar=='Never-married'){
    return(mar)
    
    #Married
  }else{
    return('Married')
  }
}

adult$marital <- sapply(adult$marital,group_marital)

### Country Grouping ####

table(adult$country)
levels(adult$country)

Asia <- c('China','Hong','India','Iran','Cambodia','Japan', 'Laos' ,
          'Philippines' ,'Vietnam' ,'Taiwan', 'Thailand')

North.America <- c('Canada','United-States','Puerto-Rico' )

Europe <- c('England' ,'France', 'Germany' ,'Greece','Holand-Netherlands','Hungary',
            'Ireland','Italy','Poland','Portugal','Scotland','Yugoslavia')

Latin.and.South.America <- c('Columbia','Cuba','Dominican-Republic','Ecuador',
                             'El-Salvador','Guatemala','Haiti','Honduras',
                             'Mexico','Nicaragua','Outlying-US(Guam-USVI-etc)','Peru',
                             'Jamaica','Trinadad&Tobago')
Other <- c('South')

group_country <- function(ctry){
  if (ctry %in% Asia){
    return('Asia')
  }else if (ctry %in% North.America){
    return('North.America')
  }else if (ctry %in% Europe){
    return('Europe')
  }else if (ctry %in% Latin.and.South.America){
    return('Latin.and.South.America')
  }else{
    return('Other')      
  }
}

adult$country <- sapply(adult$country,group_country)
table(adult$country)

## Munging adult dataframe ##
adult[adult=='?'] <- NA
print(table(adult$type_employer))

### Factorize String categories
adult$type_employer <- sapply(adult$type_employer,factor)
adult$country <- sapply(adult$country,factor)
adult$marital <- sapply(adult$marital,factor)
adult$occupation <- sapply(adult$occupation,factor)

## Check for empty records ####
missmap(adult, y.at=c(1), y.labels = c(''), col = c('red','black'))

## Drop missing data & check again
adult <- na.omit(adult)
missmap(adult, y.at=c(1), y.labels = c(''), col = c('red','black'))

#### Visualize the data ####
ggplot(adult,aes(age)) + geom_histogram(aes(fill = income), color = 'black', binwidth = 1) + theme_bw()
ggplot(adult,aes(hr_per_week)) + geom_histogram() + theme_bw()
ggplot(adult,aes(country)) + geom_bar(aes(fill = income), color = 'black') + theme(axis.text.x = element_text(angle = 90, hjust = 1)) + theme_bw()

#### Train Test Split Model ####
#### Train Logistic Regression Model

set.seed(101)
split = sample.split(adult$income, SplitRatio = 0.70)
adult.train = subset(adult, split == TRUE)
adult.test = subset(adult, split == FALSE)

regression.model <- glm(formula=income ~ . , family = binomial(link='logit'),data = adult.train)
summary(regression.model)

adult.test$predicted.income <- predict(regression.model, newdata = adult.test, type = 'response')
table(adult.test$income, adult.test$predicted.income > 0.5)

#### Accuracy ####
(6372+1423)/(6372+1423+548+872)

#### Precision & Recall ####
Precision <- 1423 / (1423 + 548)
Recall <- 1423 / (1423 + 872) 
