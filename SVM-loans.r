## Objective is to create a model that will help to predict if an investor 
## would want to invest in people who have high probability of paying back.

library(ggplot2)
## Load data and check dataframe
loans <- read.csv("loan_data.csv")
print(str(loans))
summary(loans)
head(structure(loans))

# Convert the following columns to categorical data 
columns <- c('inq.last.6mths' ,'delinq.2yrs','pub.rec' ,'not.fully.paid', 'credit.policy')
loans[,columns] <- lapply(loans[,columns] , factor)
str(loans)

# Create a histogram of fico scores colored by not.fully.paid
plt <- ggplot(loans, aes(x = fico))
plt <- plt + geom_bar(aes(fill = not.fully.paid),bins = 40, alpha = 0.5) 
plt + scale_fill_manual(values = c('green', 'red')) + theme_bw()

# Create a barplot of purpose counts, colored by not.fully.paid.
plt <- ggplot(loans, aes(x = factor(purpose)))
plt <- plt + geom_bar(aes(fill = not.fully.paid),position="dodge", bins = 40, alpha = 0.5) 
plt + scale_fill_manual(values = c('red', 'cyan')) + theme_bw()


# Build the model
#################

# Split data into train and test sets
library(caTools)
library(e1071)
set.seed(101)

spl = sample.split(loans$not.fully.paid, 0.7)
train = subset(loans, spl == TRUE)
test = subset(loans, spl == FALSE)

model <- svm(not.fully.paid ~ ., data = train)
summary(model)

pred.values <- predict(model, test[1:13])
table(pred.values, test$not.fully.paid)

tuned.results <-tune(svm, train.x = not.fully.paid ~ ., data = train,
                     kernel = 'radial', range = list(cost=c(100,200), gamma = c(0.1)))

