## Kmeans Clustering for unsupervised learning problem
library(ggplot2)
df1 <- read.csv('data/winequality-red.csv',sep=';')
df2 <- read.csv('data/winequality-white.csv',sep=';')
head(wine)

# Add wine label in each df and merge to a new one
df1$label <- 'red'
df2$label <- 'white'
wine <- rbind(df1,df2)

# Create some histograms
ggplot(wine, aes(x = residual.sugar, color=label)) + 
                     geom_histogram(aes(fill=label),bins = 50,color = 'black') +
                     scale_fill_manual(values = c('#ae4444','#faf7ea')) + theme_bw()

ggplot(wine, aes(x = citric.acid, color=label)) + 
       geom_histogram(aes(fill=label),bins = 50,color = 'black') +
       scale_fill_manual(values = c('#ae4444','#faf7ea')) + theme_bw()

ggplot(wine, aes(x = alcohol, color=label)) + 
      geom_histogram(aes(fill=label),bins = 50,color = 'black') +
      scale_fill_manual(values = c('#ae4444','#faf7ea')) + theme_bw()


ggplot(wine, aes(x = citric.acid, y = residual.sugar , color = label)) + 
      geom_point(alpha=0.2) +
      scale_color_manual(values = c('#ae4444','#faf7ea')) + theme_dark()

ggplot(wine, aes(x = volatile.acidity, y = residual.sugar , color = label)) + 
  geom_point(alpha=0.2) +
  scale_color_manual(values = c('#ae4444','#faf7ea')) + theme_dark()


clus.data <- wine[, 1:12]
wine.cluster <- kmeans(clus.data,2)
print(wine.cluster$centers)
