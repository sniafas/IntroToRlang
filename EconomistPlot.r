## Economist Assignment Data
library(ggplot2)
library(ggthemes)
library(data.table)
## Load data and check dataframe
df <- fread("data/Economist_Assignment_Data.csv",drop = 1)
head(df)

pointsToLabel <- c("Russia", "Venezuela", "Iraq", "Myanmar", "Sudan",
                   "Afghanistan", "Congo", "Greece", "Argentina", "Brazil",
                   "India", "Italy", "China", "South Africa", "Spane",
                   "Botswana", "Cape Verde", "Bhutan", "Rwanda", "France",
                   "United States", "Germany", "Britain", "Barbados", "Norway", "Japan",
                   "New Zealand", "Singapore")

subdata <- subset(df, Country %in% pointsToLabel)

pl <- ggplot(df, aes(x = CPI, y = HDI,color=Region)) + 
            geom_point(size = 4, shape = 1) +
            geom_smooth(aes(group=1), method = 'lm', formula = y ~ log(x), se = FALSE, color = 'red') +
            geom_text(aes(label = Country), color = "gray20", data = subdata, check_overlap = TRUE) + 
            scale_x_continuous(name = "Corruption Perceptions Index, 2011 (10=least corrupt)", limits = c(.9, 10.5), breaks = 1:10) + 
            scale_y_continuous(name = "Human Development Index, 2011 (1=best)", limits = c(0.2,1.0)) +
            ggtitle("Corruption and Human development") +
            theme_economist_white() + theme_bw()
